var assert = require('assert');

const { getUserDataIp } = require('../src/services/getUserDataIpService');
const { getWeather, getWeatherForecast } = require('../src/services/weatherService');

describe('Test services', () => {

  describe('getWeather service', () => {
    it('Test getUserDataIp service and get user city', (done) => {
      getUserDataIp().then((response) => {
        const { city } = response.data;
      
        assert(city);
        done();
      })
    })
  })

  describe('Test getWeather service', () => {
    it('Test getWeather service with city name Buenos Aires', (done) => {
      const cityName = 'Buenos Aires';
  
      getWeather(cityName)
        .then((response) => {
          const { weather, main, name } = response.data;
        
          assert(main, weather);
          assert.equal(name, cityName);
          done();
        });
    })

    it('Test getWeather service with parameter city that does not exist is undefined', (done) => {
      const cityName = 'abssbsaasssd';
  
      getWeather(cityName)
        .then((response) => {
          const { data } = response;
        
          assert.equal(data, undefined);
          done();
        });
    })

    it('Test getWeather service without city parameter', (done) => {  
      getWeather()
        .catch((error) => {
          const { message } = error;
        
          assert.equal(message, 'City ​​parameter not found');
          done();
        })
    })
  })

  describe('Test getWeatherForecast service', () => {
    it('Test getWeatherForecast service with city name Buenos Aires', (done) => {
      const cityName = 'Buenos Aires';
  
      getWeatherForecast(cityName)
        .then((response) => {
          const { allWeatherData, oneWeatherPerDay } = response;
        
          assert(allWeatherData, oneWeatherPerDay);
          done();
        });
    })

    it('Test getWeatherForecast service with parameter city that does not exist is undefined', (done) => {
      const cityName = 'abssbsaasssd';

      getWeatherForecast(cityName)
        .catch((error) => {

          assert.equal(error.message, 'City weather not found');
          done();
        })
    })

    it('Test getWeatherForecast service without city parameter', (done) => {  
      getWeatherForecast()
        .catch((error) => {
          const { message } = error;
        
          assert.equal(message, 'City ​​parameter not found');
          done();
        })
    })
  })

});
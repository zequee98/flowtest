var assert = require('assert');

const { WEATHER_KEY, MY_PUBLIC_IP_ADDRESS } = require('../src/config');

describe('Environment test', () => {

  it('WEATHER_KEY exists in "src/config"', (done) => {
    if (WEATHER_KEY) {
      done();
    } else {
      done('WEATHER_KEY does not exist in the configuration file')
    }
  })

  it('MY_PUBLIC_IP_ADDRESS exists in "src/config"', (done) => {
    if (MY_PUBLIC_IP_ADDRESS) {
      done();
    } else {
      done('MY_PUBLIC_IP_ADDRESS does not exist in the configuration file')
    }
  })
});
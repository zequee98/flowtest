var assert = require('assert');
const request = require('supertest');

const { VERSION } = require('../src/config');
const app = require('../src/server').boostrapApp();
const { getUserDataIp } = require('../src/services/getUserDataIpService');

describe('Route test: /current', function () {

  describe('Successful api response', function () {
    it('Test without parameter and get the results: weather and main', function (done) {
      request(app)
        .get(`/${VERSION}/current/`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(async (response) => {
          const { weather, cityName, temp } = response.body;

          if (weather && cityName && temp) {
            done();
          }
        })
    });

    it('Test without parameter and the city obtained by the IP service coincides with the city brought by the climate service', function (done) {
      request(app)
        .get(`/${VERSION}/current/`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(async (response) => {
          const { cityName } = response.body;
          const userData = await getUserDataIp();
          const { city } = userData.data;

          assert.equal(city, cityName);
          done();
        })
    });

    it('Test with parameter: Recoleta, the name of the city is equal to the parameter sent', function (done) {
      const cityName = 'Recoleta';

      request(app)
        .get(`/${VERSION}/current/${cityName}`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(async (response) => {
          assert.equal(response.body.cityName, cityName);
          done();
        })
    });
  })

  describe('Fail api response', function () {
    it('Test with parameter that does not exist', function (done) {
      const cityName = 'abssbsaasssd';

      request(app)
        .get(`/${VERSION}/current/${cityName}`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(404)
        .then(async (response) => {
          const { message } = response.body;
  
          assert.equal(message, 'Weather data not found');
          done();
        })
    });
  
    it('Test with parameter: Number return 400', function (done) {
      const number = 22;

      request(app)
        .get(`/${VERSION}/current/${number}`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .then(async (response) => {
          const { message } = response.body;
  
          assert.equal(message, 'The city parameter cannot be a numeric value');
          done();
        })
    });
  })

});
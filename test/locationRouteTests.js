var assert = require('assert');
const request = require('supertest');

const { VERSION } = require('../src/config');
const app = require('../src/server').boostrapApp();
const { getUserDataIp } = require('../src/services/getUserDataIpService');

describe('Route test: /location', () => {

  describe('Successful api response', () => {
    it('Get the results: weather and main', (done) => {
      request(app)
        .get(`/${VERSION}/location/`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(async (response) => {
          const { weather, cityName, temp } = response.body;

          if (weather && cityName && temp) {
            done();
          }
        })
    });

    it('The city obtained by the IP service coincides with the city brought by the climate service', (done) => {
      request(app)
        .get(`/${VERSION}/location/`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(async (response) => {
          const userData = await getUserDataIp();
          const { name } = response.body;
          const { cityName } = userData.data;

          assert.equal(cityName, name);
          done();
        })
    });
  })

});
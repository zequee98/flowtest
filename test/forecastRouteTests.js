var assert = require('assert');
const request = require('supertest');

const { VERSION } = require('../src/config');
const app = require('../src/server').boostrapApp();
const { getUserDataIp } = require('../src/services/getUserDataIpService');

describe('Route test: /forecast', function () {

  describe('Successful api response', function () {
    it('Get the results: allWeatherData, oneWeatherPerDay and city', function (done) {
      request(app)
        .get(`/${VERSION}/forecast/`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(async (response) => {
          const { allWeatherData, oneWeatherPerDay, cityName } = response.body;

          if (allWeatherData && oneWeatherPerDay && cityName) {
            done();
          }
        })
    });

    it('The city obtained by the IP service coincides with the city brought by the climate service', function (done) {
      request(app)
        .get(`/${VERSION}/forecast/`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(async (response) => {
          const { cityName } = response.body;
          const userData = await getUserDataIp();

          assert.equal(cityName, userData.data.city);
          done();
        })
    });

    it('Test with parameter: Recoleta, the name of the city is equal to the parameter sent', function (done) {
      const cityName = 'Recoleta';

      request(app)
        .get(`/${VERSION}/forecast/${cityName}`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .then(async (response) => {
          const { cityName } = response.body;

          assert.equal(cityName, cityName);
          done();
        })
    });
  })

  describe('Fail api response', function () {
    it('Test with parameter that does not exist', function (done) {
      const cityName = 'abssbsaasssd';

      request(app)
        .get(`/${VERSION}/forecast/${cityName}`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(404)
        .then((response) => {
          const { message } = response.body;

          assert.equal(message, 'Weather data not found');

          done();
        })
    });

    it('Test with parameter: Number', function (done) {
      const number = 22;

      request(app)
        .get(`/${VERSION}/forecast/22`)
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(400)
        .then(async (response) => {
          const { message } = response.body;

          assert.equal(message, 'The city parameter cannot be a numeric value');
          done();
        })
    });
  })

});
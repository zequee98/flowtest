# Flow test Backend

## Antes de inciar la app se necesitan las siguientes credenciales en el siguiente archivo:

```src\config\index.js```

- WEATHER_KEY - (Key openweathermap)

- MY_PUBLIC_IP_ADDRESS (https://www.whatismyip.com/what-is-my-public-ip-address/)

Detalle sobre MY_PUBLIC_IP_ADDRESS, por lo que tengo entendido, ya que estamos corriendo la app sobre nuestro 'localhost' no tenemos acceso a la direccion IP, de igual manera deje en el archivo ```src\services\getUserDataIpService.js``` como seria si estaria en produccion (Sacamos  MY_PUBLIC_IP_ADDRESS).

## Iniciando

- npm install

- npm run test

- npm start (si desean probar con el frontend)

Es la primera vez que realizo Tests, asi que espero que este lo mas aceptable posible!

Gracias por la oportunidad!
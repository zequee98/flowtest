const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const { PORT, VERSION } = require('../config')
const routes = require('../routes');

const boostrapApp = () => {
  const app = express();

  // CORS
  app.use(cors());

  // Parse application/x-www-form-urlencoded
  app.use(bodyParser.urlencoded({ extended: false }));

  // Parse application/json
  app.use(bodyParser.json());

  // Routes
  app.use(`/${VERSION}`, routes);

  return app;
}

const server = {
  start: (onStart) => {
    const app = boostrapApp();

    app.listen(PORT, () => onStart(PORT));
  },
  boostrapApp
}

module.exports = server;

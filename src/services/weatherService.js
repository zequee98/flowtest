const axios = require('axios');

const { WEATHER_KEY } = require('../config');

const getWeather = async (city) => {
  if (city) {
    try {
      return await axios.get(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${WEATHER_KEY}`);
    } catch (error) {
      return error;
    }
  } else {
    throw ({ message: 'City ​​parameter not found' });
  }
}

const getWeatherForecast = async (userCity) => {
  if (userCity) {
    try {
      const listWeather = await axios.get(`http://api.openweathermap.org/data/2.5/forecast?q=${userCity}&appid=${WEATHER_KEY}`);
      const { list, city } = listWeather.data;

      // Obtengo la data de cada dia en un determinado horario.
      const oneWeatherPerDay = [list[7], list[15], list[23], list[31], list[39]];

      return { allWeatherData: list, oneWeatherPerDay, cityName: city.name };
    } catch {
      throw ({ message: 'City weather not found' });
    }
  } else {
    throw ({ message: 'City ​​parameter not found' });
  }
}

module.exports = {
  getWeather,
  getWeatherForecast
}
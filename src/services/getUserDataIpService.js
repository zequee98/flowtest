const axios = require('axios');
const { MY_PUBLIC_IP_ADDRESS } = require('../config');

const getUserDataIp = async (req) => {
  try {
    const userIp = MY_PUBLIC_IP_ADDRESS || req.headers['x-forwarded-for'] || req.connection.remoteAddress;

    return await axios.get(`http://ip-api.com/json/${userIp}`);
  } catch (error) {
    return error;
  }
}

module.exports = {
  getUserDataIp
};
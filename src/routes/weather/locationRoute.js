const router = require('express').Router();

const { getWeather } = require('../../services/weatherService');
const { getUserDataIp } = require('../../services/getUserDataIpService');

router.get('/', async (req, res) => {
  try {
    const userData = await getUserDataIp(req);
    const weather = await getWeather(userData.data.city);

    if (weather.data) {
      res.status(200).send({
        weather: weather.data.weather[0],
        cityName: weather.data.name,
        temp: weather.data.main,
      });
    } else {
      res.status(404).send({ message: 'Weather data not found' });
    }
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;

const router = require('express').Router();

const { getWeather } = require('../../services/weatherService');
const { getUserDataIp } = require('../../services/getUserDataIpService');

router.get('/:city?', async (req, res) => {
  const { city } = req.params;

  if (isNaN(Number(city))) {
    try {
      let weather;

      if (city) {
        weather = await getWeather(city);
      } else {
        const userData = await getUserDataIp(req);
        weather = await getWeather(userData.data.city);
      }

      if (weather.data) {
        res
          .status(200)
          .send({
            weather: weather.data.weather[0],
            cityName: weather.data.name,
            temp: weather.data.main,
          });
      } else {
        res.status(404).send({ message: 'Weather data not found' });
      }
    } catch (error) {
      res.status(500).send(error);
    }
  } else {
    res.status(400).send({ message: 'The city parameter cannot be a numeric value' });
  }
});

module.exports = router;

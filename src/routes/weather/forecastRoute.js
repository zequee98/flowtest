const router = require('express').Router();

const { getWeatherForecast } = require('../../services/weatherService');
const { getUserDataIp } = require('../../services/getUserDataIpService');

router.get('/:city?', async (req, res) => {
  const { city } = req.params;

  if (isNaN(Number(city))) {
    try {
      let weatherInfo;

      if (city) {
        try {
          weatherInfo = await getWeatherForecast(city);
        } catch (error) {
          return res.status(404).send({ message: 'Weather data not found' });
        }
      } else {
        const userData = await getUserDataIp(req);
        const { data } = userData;

        weatherInfo = await getWeatherForecast(data.city);
      }

      res.status(200).send(weatherInfo);
    } catch (error) {
      res.status(500).send(error);
    }
  } else {
    res.status(400).send({ message: 'The city parameter cannot be a numeric value' });
  }
});

module.exports = router;

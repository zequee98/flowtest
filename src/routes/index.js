const router = require('express').Router();

const Location = require('./weather/locationRoute');
const Current = require('./weather/currentRoute');
const Forecast = require('./weather/forecastRoute');

router.use('/location', Location);
router.use('/current', Current);
router.use('/forecast', Forecast);

module.exports = router;
